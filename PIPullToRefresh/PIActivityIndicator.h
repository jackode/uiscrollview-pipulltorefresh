//
//  PIActivityIndicator.h
//  NewPiki
//
//  Created by Pham Quy on 1/27/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PIActivityIndicatorProtocol <NSObject>
@optional
@property (nonatomic) NSUInteger frameRate;
- (void) updateViewForProgress:(CGFloat) progress;
- (void) startAnimationLoopCount:(NSInteger) loopCount;
- (void) stopAnimation;
@end


@interface PIImageActivityIndicator : UIView <PIActivityIndicatorProtocol>
- (instancetype) initWithImages:(NSArray*) images;
- (instancetype) initWithGifFile:(NSString*) filePath;
@end
