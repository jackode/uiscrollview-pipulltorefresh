//
//  PIActivityIndicator.m
//  NewPiki
//
//  Created by Pham Quy on 1/27/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import "PIActivityIndicator.h"
@interface PIImageActivityIndicator()
@property (nonatomic, strong) NSArray* images;
@property (nonatomic, strong) UIImageView* imageView;
@end



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

@implementation PIImageActivityIndicator
@synthesize frameRate=_frameRate;

- (instancetype) initWithGifFile:(NSString*) filePath
{
    UIImage *progressImage = [[UIImage alloc] initWithContentsOfFile:filePath];
    return [self initWithImages:progressImage.images];
}


- (instancetype) initWithImages:(NSArray*) images
{
    self = [super init];
    if (self) {
        if(self) {
            self.images = images;
            UIImageView* imageView = [[UIImageView alloc] init];
            [imageView setContentMode:(UIViewContentModeCenter)];
            imageView.translatesAutoresizingMaskIntoConstraints = NO;
            [self addSubview:imageView];
            
            NSDictionary* viewDicts = @{@"imageView": imageView};
            NSMutableArray* layoutConstraints = [NSMutableArray array];
            
            [layoutConstraints
             addObjectsFromArray: [NSLayoutConstraint
                                   constraintsWithVisualFormat:@"|-0-[imageView]-0-|"
                                   options:0
                                   metrics:nil
                                   views:viewDicts]];
            
            [layoutConstraints
             addObjectsFromArray: [NSLayoutConstraint
                                   constraintsWithVisualFormat:@"V:|-0-[imageView]-0-|"
                                   options:0
                                   metrics:nil
                                   views:viewDicts]];
            
            [self addConstraints:layoutConstraints];
            self.imageView = imageView;
        }
    }
    return self;
}



//------------------------------------------------------------------------------
#pragma mark - PIActivityIndicatorProtocol
//------------------------------------------------------------------------------

- (void) updateViewForProgress:(CGFloat) progress
{
    // Boundary guard
//    progress = MIN(progress, 1.);
//    progress = MAX(0., progress);

    //Animation
    NSInteger index = (NSInteger)roundf((self.images.count ) * progress);
    index = index % self.images.count; //MIN(index, self.images.count-1);
    index = MAX(0, index);;
    
    self.imageView.animationImages = nil;
    self.imageView.image = [self.images objectAtIndex:index];
    
}

//------------------------------------------------------------------------------
- (void) startAnimationLoopCount:(NSInteger) loopCount
{
    self.imageView.image = nil;
    self.imageView.animationImages = self.images;
    
    NSUInteger frameRate = self.frameRate > 0 ? self.frameRate : 30;
    
    self.imageView.animationDuration =  self.images.count/frameRate;
    NSUInteger repeat = MAX(0, loopCount);
    self.imageView.animationRepeatCount = repeat;
    [self.imageView startAnimating];
}

//------------------------------------------------------------------------------
- (void) stopAnimation
{
    [self.imageView stopAnimating];
    self.imageView.image = self.images.firstObject;
    self.imageView.animationImages = nil;
}
@end
