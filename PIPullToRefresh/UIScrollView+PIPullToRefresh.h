//
//  UIScrollView+UIScrollView_PIPullToRefresh.h
//  NewPiki
//
//  Created by Pham Quy on 1/27/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PIActivityIndicatorProtocol;


typedef NS_ENUM(NSInteger, PIPullToRefreshSide)
{
    PIPullToRefreshSideTop,
    PIPullToRefreshSideBottom
};

@interface UIScrollView (PIPullToRefresh)
@property (nonatomic) BOOL enablePullToRefresh;

- (void) addPullToFreshActionBlock:(void(^)(void)) actionBlock
                    progressImages:(NSArray*)progressImages
                     loadingImages:(NSArray*)loadingImages
                     pullThreshold:(CGFloat) thresold
             loadingImageFrameRate:(NSUInteger) fps;


- (void) addPullToFreshActionBlock:(void(^)(void)) actionBlock
                   progressGifFile:(NSString*)progressGif
                    loadingGifFile:(NSString*)loadingGif
                     pullThreshold:(CGFloat) thresold
             loadingImageFrameRate:(NSUInteger) fps;


- (void) addPullToFreshActionBlock:(void(^)(void)) actionBlock
                 progressIndicator:(UIView<PIActivityIndicatorProtocol>*) progressIndicator
                  loadingIndicator:(UIView<PIActivityIndicatorProtocol>*) loadingIndicator
                     pullThreshold:(CGFloat) thresold;

- (void) removePullToRefresh;
- (void) stopFullToRefreshAnimation;
@end
