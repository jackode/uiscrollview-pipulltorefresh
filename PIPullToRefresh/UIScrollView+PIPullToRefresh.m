//
//  UIScrollView+UIScrollView_PIPullToRefresh.m
//  NewPiki
//
//  Created by Pham Quy on 1/27/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import "UIScrollView+PIPullToRefresh.h"
#import "PIActivityIndicator.h"
#import "TransitionKit.h"



#import <objc/runtime.h>
#define SYNTHESIZE_CATEGORY_OBJ_PROPERTY(propertyGetter, propertySetter)                                                             \
- (id) propertyGetter {                                                                                                             \
return objc_getAssociatedObject(self, @selector( propertyGetter ));                                                             \
}                                                                                                                                   \
- (void) propertySetter (id)obj{                                                                                                    \
objc_setAssociatedObject(self, @selector( propertyGetter ), obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                            \
}


#define SYNTHESIZE_CATEGORY_VALUE_PROPERTY(valueType, propertyGetter, propertySetter)                                                \
- (valueType) propertyGetter {                                                                                                      \
valueType ret = {0};                                                                                                                  \
[objc_getAssociatedObject(self, @selector( propertyGetter )) getValue:&ret];                                                    \
return ret;                                                                                                                     \
}                                                                                                                                   \
- (void) propertySetter (valueType)value{                                                                                           \
NSValue *valueObj = [NSValue valueWithBytes:&value objCType:@encode(valueType)];                                                \
objc_setAssociatedObject(self, @selector( propertyGetter ), valueObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                       \
}



#define  _PIPullStateNone          @"_PIPullStateNone"
#define  _PIPullStatePulling       @"_PIPullStatePulling"
#define  _PIPullStateCanTrigger    @"_PIPullStateCanTrigger"
#define  _PIPullStateTriggered     @"_PIPullStateTriggered"


#define _PIPullEventStartPulling            @"_PIPullEventStartPulling"
#define _PIPullEventCancelPulling           @"_PIPullEventCancelPulling"
#define _PIPullEventCrossOverThreshold      @"_PIPullEventCrossOverThreshold"
#define _PIPullEventCrossUnderThreshold     @"_PIPullEventCrossUnderThreshold"
#define _PIPullEventTrigger                 @"_PIPullEventTrigger"
#define _PIPullEventDoneRefresh             @"_PIPullEventDoneRefresh"


@interface _PIPullToRefreshController : NSObject
@property (nonatomic,strong) UIView* indicatorContainer;
@property (nonatomic,strong) UIView<PIActivityIndicatorProtocol>* progressIndicator;
@property (nonatomic,strong) UIView<PIActivityIndicatorProtocol>* loadingIndicator;
@property (nonatomic,assign) CGFloat threshold;
@property (nonatomic,assign) NSUInteger frameRate;
@property (nonatomic,strong) void(^actionBlock)();
@property (nonatomic) BOOL internalEnable;
@property (nonatomic) PIPullToRefreshSide side;
@property (nonatomic) BOOL enable;
@property (nonatomic, weak) UIScrollView* scrollView;
@property (nonatomic) UIEdgeInsets originalInset;
@property (nonatomic, strong) TKStateMachine *pullStateMachine;

- (void) stopCurrentPullToRefresh;
@end


@implementation _PIPullToRefreshController

- (instancetype) initWithScrollView:(UIScrollView*) scrollView
{
    self = [super init];
    if (self) {
        self.scrollView = scrollView;
        [self initStateMachine];
    }
    return self;
}

//------------------------------------------------------------------------------

- (void) initStateMachine
{
    TKStateMachine *pullStateMachine = [TKStateMachine new];
    
    TKState *initState = [TKState stateWithName:_PIPullStateNone];
    [initState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        UIEdgeInsets loadingInset = _originalInset;
        [self.loadingIndicator stopAnimation];
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             [self.scrollView setContentInset:loadingInset];
                             [self.scrollView setContentOffset:CGPointMake(0, -loadingInset.top)];
                         }
                         completion:^(BOOL finished) {
//                             self.progressIndicator.hidden = YES;
                         }];

        
        NSLog(@"%@--->%@", transition.sourceState.name, transition.destinationState.name);
    }];
    
    TKState *pullingState = [TKState stateWithName:_PIPullStatePulling];
    [pullingState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        NSLog(@"%@--->%@", transition.sourceState.name, transition.destinationState.name);
        self.indicatorContainer.hidden = NO;
        self.progressIndicator.hidden = NO;
        self.loadingIndicator.hidden = YES;
    }];
    
    TKState *canTriggerState = [TKState stateWithName:_PIPullStateCanTrigger];
    [canTriggerState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        NSLog(@"%@--->%@", transition.sourceState.name, transition.destinationState.name);
    }];
    
    TKState *triggeredState = [TKState stateWithName:_PIPullStateTriggered];
    [triggeredState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        NSLog(@"%@--->%@", transition.sourceState.name, transition.destinationState.name);
        self.progressIndicator.hidden = YES;
        self.loadingIndicator.hidden = NO;
        [self.loadingIndicator startAnimationLoopCount:0];
        UIEdgeInsets loadingInset = _originalInset;
        loadingInset.top += _threshold;
        
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             [self.scrollView setContentInset:loadingInset];
                             [self.scrollView setContentOffset:CGPointMake(0, -loadingInset.top)];
                         }
                         completion:^(BOOL finished) {
                             NSLog(@"Did scroll to loading position");
                             if (self.actionBlock) {
                                 self.actionBlock();
                             }
                         }];

        NSLog(@"Scroll to loading position");
    }];
    
    [pullStateMachine addStates:@[ initState, pullingState, canTriggerState, triggeredState]];
    pullStateMachine.initialState = initState;
    
    TKEvent *startScroll = [TKEvent eventWithName:_PIPullEventStartPulling
                          transitioningFromStates:@[ initState ]
                                          toState:pullingState];
    
    TKEvent *cancelScroll = [TKEvent eventWithName:_PIPullEventCancelPulling
                          transitioningFromStates:@[ pullingState ]
                                          toState:initState];

    
    TKEvent *crossOver = [TKEvent eventWithName:_PIPullEventCrossOverThreshold
                             transitioningFromStates:@[ pullingState ]
                                             toState: canTriggerState];

    TKEvent *crossUnder = [TKEvent eventWithName:_PIPullEventCrossUnderThreshold
                             transitioningFromStates:@[ canTriggerState ]
                                             toState: pullingState];

    TKEvent *releaseScroll = [TKEvent eventWithName:_PIPullEventTrigger
                            transitioningFromStates:@[ canTriggerState ]
                                            toState: triggeredState];
    
    TKEvent* finishLoading = [TKEvent eventWithName:_PIPullEventDoneRefresh
                            transitioningFromStates:@[ triggeredState ]
                                            toState:initState];
    
    [pullStateMachine addEvents:@[ startScroll, cancelScroll, crossOver, crossUnder, releaseScroll, finishLoading]];
    
    pullStateMachine.initialState = initState;
    // Activate the state machine
    [pullStateMachine activate];
    
    self.pullStateMachine = pullStateMachine;
}
//------------------------------------------------------------------------------
#pragma mark - Accessor
- (void) setScrollView:(UIScrollView *)scrollView
{
    _scrollView = scrollView;
    _originalInset = _scrollView.contentInset;
}
//------------------------------------------------------------------------------
- (void) setEnable:(BOOL)enable
{
    NSAssert(_scrollView, @"No scroll view registered");
    if (self.enable != enable) {
        _enable = enable;
        if(enable)
        {
            
            CGFloat indicatorTop = 0;
            if (self.side == PIPullToRefreshSideTop) {
                indicatorTop = -(_scrollView.contentInset.top + self.threshold);
            }else{
                indicatorTop = _scrollView.contentSize.height + _scrollView.contentInset.bottom;
            }
            
            self.indicatorContainer.frame = CGRectMake(0, indicatorTop,
                                                       self.indicatorContainer.frame.size.width,
                                                       self.threshold);
            
            [_scrollView addSubview:self.indicatorContainer];
            
            [_scrollView addObserver:self
                   forKeyPath:@"contentOffset"
                      options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                      context:NULL];
            
            [_scrollView addObserver:self
                   forKeyPath:@"contentSize"
                      options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                      context:NULL];
        }else{
            [_scrollView removeObserver:self forKeyPath:@"contentOffset"];
            [_scrollView removeObserver:self forKeyPath:@"contentSize"];
            [self.indicatorContainer removeFromSuperview];
            [self.scrollView setContentInset:self.originalInset];
        }
    }
    
}

- (void) dealloc
{
    [self setEnable:NO];
}
//------------------------------------------------------------------------------
#pragma mark - Handle frame & size change
//------------------------------------------------------------------------------
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    
    if ([keyPath isEqualToString:@"contentOffset"]) {
        [self handleContentOffsetChange:change];
    }
    if ([keyPath isEqualToString:@"contentSize"]) {
        [self handleContentSizeChange:change];
    }
}

//------------------------------------------------------------------------------
- (void) handleContentOffsetChange:(NSDictionary*) change
{
    CGPoint new = [[change objectForKey:NSKeyValueChangeNewKey] CGPointValue];
    
    CGPoint old = [[change objectForKey:NSKeyValueChangeOldKey] CGPointValue];
    CGFloat movement = new.y - old.y;
    
    if (fabs(movement) < FLT_EPSILON) {
        return;
    }
    
    CGFloat adjustOffset = new.y + _originalInset.top;
    //NSLog(@"%f", adjustOffset);

    CGFloat height = adjustOffset > 0 ? 0: fabs(adjustOffset);
    CGFloat top = -height-self.originalInset.top;
    CGFloat percentage = height/self.threshold;
    
    self.indicatorContainer.frame = CGRectMake(0, top, self.scrollView.frame.size.width, height);
    
    NSError* error = nil;
    if ([_pullStateMachine.currentState.name isEqualToString:_PIPullStateNone]) {
        if (adjustOffset < 0) {
            [self.pullStateMachine fireEvent:[self.pullStateMachine eventNamed:_PIPullEventStartPulling] userInfo:nil error:&error];
        }
    }
    else if ([_pullStateMachine.currentState.name isEqualToString:_PIPullStatePulling])
    {
        [self.progressIndicator updateViewForProgress:percentage];
        
        if (fabs(adjustOffset) < FLT_EPSILON) {
            [self.pullStateMachine fireEvent:[self.pullStateMachine eventNamed:_PIPullEventCancelPulling] userInfo:nil error:&error];
        }else if (fabs(adjustOffset) > self.threshold){
            [self.pullStateMachine fireEvent:[self.pullStateMachine eventNamed:_PIPullEventCrossOverThreshold] userInfo:nil error:&error];
        }
    }
    else if ([_pullStateMachine.currentState.name isEqualToString:_PIPullStateCanTrigger])
    {
        [self.progressIndicator updateViewForProgress:percentage];
        if (!self.scrollView.isDragging) {
            [self.pullStateMachine fireEvent:[self.pullStateMachine eventNamed:_PIPullEventTrigger] userInfo:nil error:&error];
        }else{
            if (fabs(adjustOffset) < self.threshold ) {
                [self.pullStateMachine fireEvent:[self.pullStateMachine eventNamed:_PIPullEventCrossUnderThreshold] userInfo:nil error:&error];
            }
        }
    }
    else if([_pullStateMachine.currentState.name isEqualToString:_PIPullStateTriggered])
    {
        
    }
    
}
//------------------------------------------------------------------------------
- (void) handleContentSizeChange:(NSDictionary*) change
{
    
}

- (void) stopCurrentPullToRefresh
{
    if ([self.pullStateMachine canFireEvent:_PIPullEventDoneRefresh]) {
        [self.pullStateMachine fireEvent:_PIPullEventDoneRefresh
                                userInfo:nil error:nil];
    }
}
@end
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

@interface UIScrollView ()
@property (nonatomic, strong) _PIPullToRefreshController* pikiScrollviewPullToRefreshController;
@end

@implementation UIScrollView (PIPullToRefresh)
SYNTHESIZE_CATEGORY_OBJ_PROPERTY(pikiScrollviewPullToRefreshController, setPikiScrollviewPullToRefreshController:)

- (void) addPullToFreshActionBlock:(void(^)(void)) actionBlock
                    progressImages:(NSArray*)progressImages
                     loadingImages:(NSArray*)loadingImages
                     pullThreshold:(CGFloat) threshold
             loadingImageFrameRate:(NSUInteger) fps
{
    PIImageActivityIndicator* pIndicator = [[PIImageActivityIndicator alloc]
                                            initWithImages:progressImages];
    PIImageActivityIndicator* lIndicator = [[PIImageActivityIndicator alloc]
                                            initWithImages:loadingImages];
    [lIndicator setFrameRate:fps];
    
    
    [self addPullToFreshActionBlock:actionBlock
                  progressIndicator:pIndicator
                   loadingIndicator:lIndicator
                      pullThreshold:threshold];
    
}

//------------------------------------------------------------------------------
- (void) addPullToFreshActionBlock:(void(^)(void)) actionBlock
                   progressGifFile:(NSString*)progressGif
                    loadingGifFile:(NSString*)loadingGif
                     pullThreshold:(CGFloat) threshold
             loadingImageFrameRate:(NSUInteger) fps
{
    PIImageActivityIndicator* pIndicator = [[PIImageActivityIndicator alloc]
                                            initWithGifFile:progressGif];
    PIImageActivityIndicator* lIndicator = [[PIImageActivityIndicator alloc]
                                            initWithGifFile:loadingGif];
    [lIndicator setFrameRate:fps];
    
    [self addPullToFreshActionBlock:actionBlock
                  progressIndicator:pIndicator
                   loadingIndicator:lIndicator
                      pullThreshold:threshold];
    
}

//------------------------------------------------------------------------------
- (void) addPullToFreshActionBlock:(void(^)(void)) actionBlock
                 progressIndicator:(UIView<PIActivityIndicatorProtocol>*) progressIndicator
                  loadingIndicator:(UIView<PIActivityIndicatorProtocol>*) loadingIndicator
                     pullThreshold:(CGFloat) threshold
{
    
    
    UIView* container =  [[UIView alloc ] initWithFrame:CGRectMake(0, 0, self.frame.size.width, threshold)];
    container.clipsToBounds = YES;
    [progressIndicator setFrame:container.bounds];
    [loadingIndicator setFrame:container.bounds];
    [container addSubview:progressIndicator];
    [container addSubview:loadingIndicator];
    
    progressIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    loadingIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary* viewDicts = @{@"pIndicator":progressIndicator, @"lIndicator" : loadingIndicator};
    NSMutableArray* layoutConstraints = [NSMutableArray array];
    
    [layoutConstraints
     addObjectsFromArray: [NSLayoutConstraint
                           constraintsWithVisualFormat:@"|-0-[pIndicator]-0-|"
                           options:0
                           metrics:nil
                           views:viewDicts]];
    
    
    [layoutConstraints
     addObjectsFromArray: [NSLayoutConstraint
                           constraintsWithVisualFormat:@"V:|-0-[pIndicator]-0-|"
                           options:0
                           metrics:nil
                           views:viewDicts]];
    
    [layoutConstraints
     addObjectsFromArray: [NSLayoutConstraint
                           constraintsWithVisualFormat:@"|-0-[lIndicator]-0-|"
                           options:0
                           metrics:nil
                           views:viewDicts]];
    [layoutConstraints
     addObjectsFromArray: [NSLayoutConstraint
                           constraintsWithVisualFormat:@"V:|-0-[lIndicator]-0-|"
                           options:0
                           metrics:nil
                           views:viewDicts]];
    
    [container addConstraints:layoutConstraints];
    [container setBackgroundColor:[UIColor colorWithRed:0./255. green:166./255. blue:222/255. alpha:1]];
    CGFloat pullThreshold = fabsf(threshold) < FLT_EPSILON ? 0 : threshold;
    
    _PIPullToRefreshController* controller = [[_PIPullToRefreshController alloc] initWithScrollView:self];
    [controller setThreshold:pullThreshold];
    [controller setProgressIndicator:progressIndicator];
    [controller setLoadingIndicator:loadingIndicator];
    [controller setIndicatorContainer:container];
    [controller setActionBlock:actionBlock];
    [controller setSide:(PIPullToRefreshSideTop)];
    [self setPikiScrollviewPullToRefreshController:controller];
    
    // Should call enable only fully settup
    [self setEnablePullToRefresh:YES];
}

//------------------------------------------------------------------------------
//- (void)willMoveToSuperview:(UIView *)newSuperview
//{
//    if (self.superview && newSuperview == nil) {
//        if (self.pikiScrollviewPullToRefreshController)
//        {
//            [self.pikiScrollviewPullToRefreshController setEnable:NO];
//        }
//    }
//}

//------------------------------------------------------------------------------
- (void) setEnablePullToRefresh:(BOOL)enablePullToRefresh
{
    [self.pikiScrollviewPullToRefreshController setEnable:enablePullToRefresh];
}


//------------------------------------------------------------------------------
- (BOOL) enablePullToRefresh
{
    return self.pikiScrollviewPullToRefreshController.enable;
}

- (void) removePullToRefresh
{
    if (self.pikiScrollviewPullToRefreshController)
    {
        [self.pikiScrollviewPullToRefreshController setEnable:NO];
        [self setPikiScrollviewPullToRefreshController:nil];
    }
}
//------------------------------------------------------------------------------
- (void) stopFullToRefreshAnimation
{
    [self.pikiScrollviewPullToRefreshController stopCurrentPullToRefresh];
}

@end
