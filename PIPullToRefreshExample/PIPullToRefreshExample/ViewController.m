//
//  ViewController.m
//  PIPullToRefreshExample
//
//  Created by Pham Quy on 2/5/15.
//  Copyright (c) 2015 Jkorp. All rights reserved.
//

#import "ViewController.h"
#import <PIPullToRefresh/UIScrollView+PIPullToRefresh.h>

@interface ViewController ()<UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *scrollview;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [_scrollview setBackgroundColor:[UIColor whiteColor]];
    [_scrollview setRowHeight:100];
    [_scrollview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellId"];
    _scrollview.dataSource = self;
    
    __weak typeof(self) wself = self;
    
    NSString* pGif = [[[NSBundle mainBundle] resourcePath]  stringByAppendingPathComponent:@"sample.gif"];
    NSString* lGif = [[[NSBundle mainBundle] resourcePath]  stringByAppendingPathComponent:@"run.gif"];
    [_scrollview
     addPullToFreshActionBlock:^{
         [wself performSelector:@selector(didRefresh) withObject:nil afterDelay:3];
     }
     progressGifFile:pGif
     loadingGifFile:lGif
     pullThreshold:100
     loadingImageFrameRate:30];

}

- (void)didRefresh {
    [self.scrollview stopFullToRefreshAnimation];
}

- (void) dealloc
{
    [self.scrollview removePullToRefresh];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell  = [tableView dequeueReusableCellWithIdentifier:@"cellId" forIndexPath:indexPath];
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    [cell.contentView setBackgroundColor:color];
    return cell;
}
@end
