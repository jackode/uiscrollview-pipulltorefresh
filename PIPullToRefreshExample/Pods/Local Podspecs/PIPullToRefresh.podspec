Pod::Spec.new do |s|

# Root specification

  s.name         = "PIPullToRefresh"
  s.version      = "0.0.1"
  s.summary      = "UIScrollView category for pull to refresh. Easy use, and customize. Gif animation support"
  s.homepage     = ""
  s.author       = { "Jack" => "psyquy@gmail.com" }
#  s.source       = {:git => 'https://jackode@bitbucket.org/jackode/nklib.git', :commit => '1df318a'}
#  s.source       = {:path => '.'}
#  s.source       = { :git => '..', :branch => 'development' }    

# Build setting
  s.dependency 'TransitionKit', '~> 2.1'
  s.dependency 'AnimatedGIFImageSerialization', '~> 0.2'
  s.ios.deployment_target = '7.0'
  s.platform = :ios, '7.0' 
  s.framework  = 'UIKit'
  s.requires_arc = true
  s.public_header_files = 'PIPullToRefresh/*.h'
  s.source_files  = 'PIPullToRefresh/**/*.{h,m}'
end